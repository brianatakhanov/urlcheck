
#!/usr/bin/python
# -*- coding: utf-8 -*-

# UrlChecker app
# Property of OpenDNS
# author: Brian Atakhanov

import os
import urlchecker 
import unittest
import tempfile
import requests

LOCALHOST= 'http://localhost:5000/' 
URL_CHECK_SERVICE = LOCALHOST + 'urlinfo/1/' 

class AppUnitTest(unittest.TestCase):
	
	def setUp(self):
		pass
	
	def tearDown(self):
		pass

	# check the existence of / page based of response.status_code
	def test_index_page_exists(self):
		r = requests.get(LOCALHOST)
		self.assertEqual(r.status_code, 200)

	# check the existence of /db page based of response.status_code
	def test_db_page_exists(self):
		r = requests.get(LOCALHOST+'db')
		self.assertEqual(r.status_code, 200)

	# very basic unit tests
	# much more details of the response can be asserted (ie: headers, json, etc)	
	def test_url_check(self):
		goodList = ['facebook.com/index.php?id=123', 'cnn.com/1/2/3/4/5']
		badList = ['123.com', 'altavista.com', 'whois.com']
		
		for url in goodList:
			r = requests.get(URL_CHECK_SERVICE + url)
			self.assertEqual(r.status_code, 200)

		for url in badList:
			r = requests.get(URL_CHECK_SERVICE + url)
			self.assertEqual(r.status_code, 404)
		

if __name__ == "__main__":
	unittest.main()
