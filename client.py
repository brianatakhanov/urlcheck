#!/usr/bin/python
# -*- coding: utf-8 -*-

# UrlChecker app
# Property of OpenDNS
# author: Brian Atakhanov

import requests

URL_CHECK_SERVICE='http://localhost:5000/urlinfo/1/'

list = ['facebook.com/index.php&id=12', 'https://www.google.ca/search?q=donald+trump','http://www.cnn.com/2016/10/17/africa/', 'poker.net/showme/the/money']

for _ in range(100): 
	for url in list:
		r = requests.get(URL_CHECK_SERVICE + url)
		print(r.json())

