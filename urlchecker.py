#!/usr/bin/python
# -*- coding: utf-8 -*-

# UrlChecker app
# Property of OpenDNS
# author: Brian Atakhanov

# Tool required:
# 0. python (sudo apt-get install 
# 1. pip (sudo easy_install pip) 
# 2. Flask (pip install flask)


from flask import Flask,jsonify,abort,request,make_response
import json,datetime 

app = Flask(__name__)

sampleList = [{ 
      'id':'1',
      'tld':'facebook.com',
      'category':'social media',
      'block':False
   },
   {  
      'id':'2',
      'tld':'google.com',
      'category':'search engine',
      'block':False
   },
   {  
      'id':'3',
      'tld':'cnn.com',
      'category':'news',
      'block':True
   },
   {  
      'id':'4',
      'tld':'poker.net',
      'category':'gambling',
      'block':True
   }
]

@app.route('/')
def index():
	return jsonify(
		{'/' : 'UrlCheck App'},
		{'/db' : 'sample dictionary', 
		'/urlinfo/1/<hostname>' : 'url lookup'}) 

@app.route('/db', methods=['GET'])
def printDb():
	return jsonify(sampleList)

@app.route('/urlinfo/1/<path:hostname_and_port>/<path:original_path_and_query_string>', methods=['GET'])

# In this exercise, a simple list lookup is used. 
# I experimented with using CouchDB and result was ok too. 
def lookup(hostname_and_port, original_path_and_query_string):
	result = {'tld' : None, 'block' : None, 'category' : None, 'timestamp' : None, 'path' : None}
	for value in sampleList:
		if value['tld'] == hostname_and_port:
			return jsonify( 
				{'tld' : value['tld'], 
				'block' : value['block'], 
				'category' : value['category'], 
				'timestamp' : datetime.datetime.now(), 
				'path' : original_path_and_query_string})
		else:
			return jsonify( 
				{'tld' : None, 
				'block' : None, 
				'category' : None, 
				'timestamp' : datetime.datetime.now(), 
				'path' : original_path_and_query_string})
			
@app.errorhandler(404)
def not_found(error):
	return make_response(jsonify({'error' : 'Not found'}), 404)

if __name__ == '__main__':
	app.run(debug=True)
